#!/usr/bin/env bash

EXECUTABLE=$1

KO=$(readelf -s ${EXECUTABLE} | grep FILE | sed '1d' | awk '{print $8}')

insmod $EXECUTABLE factorial_input=5

MODULE_MESSAGE=$(dmesg | grep "Factorial of 5 is 120")

rmmod ${KO%.c}

if [ ! -z "$MODULE_MESSAGE" ]; then
  echo "Test passed."
  exit 0
else
  echo "Test failed. Expected have '$MODULE_MESSAGE' but got emoty"
  exit 1
fi
